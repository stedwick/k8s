#!/usr/bin/env sh

export K8S_APPLY_DATE="$(date +%s)" # Restart pods even if nothing changes

function kubectlapplyfile() {
  cat "$1" | envsubst | kubectl apply -f -
}

for dir in namespaces configmaps services pvolumes pvolumeclaims deployments; do
  for fil in "$K8S_ROOT/$dir/"*.yaml; do
    case "$fil" in
      *.all.yaml|*."$RAILS_ENV".yaml)
        kubectlapplyfile $fil
    esac
  done
  [ "$1" = "slow" ] && sleep 3
done

unset K8S_APPLY_DATE

