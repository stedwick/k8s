#!/usr/bin/env sh

function _dockersh() {
  local project="$1"
  shift
  local projectenvironment="$project"-"$RAILS_ENV"

  case "$*" in
    *build*)
      for f in $(find "$K8S_ROOT/docker" -path "*/$project*" -name "pre_build_first" -print); do "$f"; done
      for f in $(find "$K8S_ROOT/docker" -path "*/$project*" -name "pre_build" -print); do "$f"; done
    ;;
  esac

  if [ -r "$K8S_ROOT/docker/$project/docker-compose.$RAILS_ENV.yaml" ]; then
    local composefile="$K8S_ROOT/docker/$project/docker-compose.$RAILS_ENV.yaml"
  else
    local composefile="$K8S_ROOT/docker/$project/docker-compose.yaml"
  fi

  COMPOSE_PROJECT_NAME="$projectenvironment" docker-compose -f "$composefile" "$@"
}

_dockersh "$@"
